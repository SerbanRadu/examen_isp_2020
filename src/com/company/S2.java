package com.company;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class S2 {

    public static void main(String args[])
    {
        JFrame f = new JFrame();
        JPanel p = new JPanel();
        f.setSize(500,300);
        p.setLayout(null);

        JTextField t1 = new JTextField();
        JTextField t2 = new JTextField();
        JTextField t3 = new JTextField();
        JButton b = new JButton("Ok");

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Integer nr1 = Integer.parseInt(t1.getText());
                Integer nr2 = Integer.parseInt(t2.getText());
                String suma = Integer.toString(nr1 + nr2);
                t3.setText(suma);
            }
        } );

        t1.setBounds(20,20,200,50);
        t2.setBounds(20,90,200,50);
        t3.setBounds(20,160,200,50);
        b.setBounds(250,180,70,30);

        p.add(t1);
        p.add(t2);
        p.add(t3);
        p.add(b);
        f.setContentPane(p);
        f.setVisible(true);
    }
}
